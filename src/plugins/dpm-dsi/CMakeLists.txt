cmake_minimum_required (VERSION 2.6)

project (dpm-dsi)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/modules/")

# Version
set(dpm-dsi_MAJOR ${dmlite_MAJOR})
set(dpm-dsi_MINOR ${dmlite_MINOR})
set(dpm-dsi_PATCH ${dmlite_PATCH})

# Lib suffix
if (CMAKE_SIZEOF_VOID_P EQUAL 4)
	set (LIB_SUFFIX "")
	set (PKG_ARCH "i386")
else (CMAKE_SIZEOF_VOID_P EQUAL 4)
	set (LIB_SUFFIX 64)
	set (PKG_ARCH "x86_64")
endif (CMAKE_SIZEOF_VOID_P EQUAL 4)

# Include path
include_directories (${DMLITE_INCLUDE_DIR}
                     ${GRIDFTP_INCLUDE_DIR}/globus
                     ${GLOBUS_CONFIG_DIR}
                     ${VOMS_INCLUDE_DIR}
)

# Subdirectories
add_subdirectory (etc)
add_subdirectory (src)
