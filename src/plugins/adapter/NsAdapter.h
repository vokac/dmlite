/// @file   NsAdapter.h
/// @brief  Adapter Plugin: Cns wrapper implementation.
/// @author Alejandro Álvarez Ayllón <aalvarez@cern.ch>
#ifndef NS_ADAPTER_H
#define	NS_ADAPTER_H

#include <dirent.h>
#include <dpns_api.h>
#include <dmlite/cpp/catalog.h>

namespace dmlite {
  
  const int kAclEntriesMax = 300;
  const int kAclSize       = 13;
  const int kCommentMax    = 255;
  // CA_MAXPATHLEN +1 as from h/Castor_limits.h
  const int kPathMax       = 1024;

  /// @brief The private structure used by NsDummy to handle
  ///        openDir/readDir/closeDir
  struct PrivateDir: public Directory {
    virtual ~PrivateDir() {};
    dpns_DIR     *dpnsDir; ///< Used for calls to the dpns API.
    ExtendedStat  stat;    ///< Where the data is actually stored.
  };

  /// Catalog implemented as a wrapper around Cns API
  class NsAdapterCatalog: public Catalog, public Authn
  {
   public:
    /// Constructor
    /// @param retryLimit Limit of retrials.
    NsAdapterCatalog(unsigned retryLimit, bool hostDnIsRoot, std::string hostDn)  ;

    /// Destructor
    ~NsAdapterCatalog();

    // Overloading
    std::string getImplId(void) const throw ();

    void setStackInstance(StackInstance* si)  ;

    SecurityContext* createSecurityContext(const SecurityCredentials&)  ;
    SecurityContext* createSecurityContext(void)  ;
    void setSecurityContext(const SecurityContext*)  ;

    void        changeDir     (const std::string&)  ;
    std::string getWorkingDir (void)                ;

    ExtendedStat extendedStat(const std::string&, bool)  ;
    ExtendedStat extendedStatByRFN(const std::string& rfn)  ;

    bool access(const std::string&, int)  ;
    bool accessReplica(const std::string&, int)  ;

    SymLink readLink(ino_t)  ;

    void addReplica   (const Replica&)  ;
    void deleteReplica(const Replica&)  ;
    std::vector<Replica> getReplicas(const std::string&)  ;

    void        symlink (const std::string&, const std::string&)  ;
    std::string readLink(const std::string& path)  ;
    
    void unlink(const std::string&)                      ;

    void create(const std::string&, mode_t)  ;

    mode_t umask   (mode_t)                           throw ();
    void   setMode (const std::string&, mode_t)        ;
    void   setOwner(const std::string&, uid_t, gid_t, bool)  ;

    void setSize    (const std::string&, size_t)  ;
    
    void setAcl(const std::string&, const Acl&)  ;

    void utime(const std::string&, const struct utimbuf*)  ;

    std::string getComment(const std::string&)  ;
    void        setComment(const std::string&,
                          const std::string&)  ;

    void setGuid(const std::string&, const std::string&)  ;

    void updateExtendedAttributes(const std::string&,
                                  const Extensible&)  ;
    
    GroupInfo getGroup   (gid_t)               ;
    GroupInfo getGroup   (const std::string&)  ;
    GroupInfo getGroup   (const std::string& key,
                          const boost::any& value)  ;
    GroupInfo newGroup   (const std::string& gname)  ;
    void      updateGroup(const GroupInfo& group)  ;
    void      deleteGroup(const std::string& groupName)  ;
    
    UserInfo  getUser   (const std::string&)  ;
    UserInfo  getUser   (const std::string& key,
                         const boost::any& value)  ;
    UserInfo  newUser   (const std::string& uname)  ;
    void      updateUser(const UserInfo& user)  ;
    void      deleteUser(const std::string& userName)  ;
    
    std::vector<GroupInfo> getGroups(void)  ;
    std::vector<UserInfo>  getUsers (void)  ;

    Directory* openDir (const std::string&)  ;
    void       closeDir(Directory*)          ;

    struct dirent* readDir (Directory*)  ;
    ExtendedStat*  readDirx(Directory*)  ;

    void makeDir(const std::string&, mode_t)  ;

    void rename     (const std::string&, const std::string&)  ;
    void removeDir  (const std::string&)                      ;

    Replica getReplicaByRFN(const std::string& rfn)         ;
    void    updateReplica(const Replica& replica)  ;
       
    void getIdMap(const std::string& userName,
                  const std::vector<std::string>& groupNames,
                  UserInfo* user,
                  std::vector<GroupInfo>* groups)  ;

   protected:
    void setDpnsApiIdentity();

    StackInstance* si_;

    unsigned    retryLimit_;
    std::string cwdPath_;

    // Need to keep this in memory, as dpns/dpm API do not make
    // copy of them (sigh)
    // setAuthorizationId does, though (don't ask me why)
    char   **fqans_;
    size_t   nFqans_;

    // Host DN is root
    bool hostDnIsRoot_;
    std::string hostDn_;

    std::string userId_;
    
    const SecurityContext* secCtx_;
  };

  class NsAdapterINode: public INode {
    public:

    /// Constructor
    NsAdapterINode(unsigned retryLimit, bool hostDnIsRoot, std::string hostDn,
        std::string dpnsHost)  ;

    /// Destructor
    ~NsAdapterINode();

    // Overloading
    std::string getImplId(void) const throw ();

    void setStackInstance(StackInstance* si)  ;
    void setSecurityContext(const SecurityContext* ctx)  ;

    void begin(void)  ;
    void commit(void)  ;
    void rollback(void)  ;

    ExtendedStat create(const ExtendedStat&)  ;

    void symlink(ino_t inode, const std::string &link)  ;

    void unlink(ino_t inode)  ;

    void move  (ino_t inode, ino_t dest)  ;
    void rename(ino_t inode, const std::string& name)  ;

    ExtendedStat extendedStat(ino_t inode)  ;
    ExtendedStat extendedStat(ino_t parent, const std::string& name)  ;
    ExtendedStat extendedStat(const std::string& guid)  ;

    SymLink readLink(ino_t inode)  ;

    void addReplica   (const Replica&)  ;  
    void deleteReplica(const Replica&)  ;

    std::vector<Replica> getReplicas(ino_t inode)  ;

    Replica getReplica   (int64_t rid)  ;
    Replica getReplica   (const std::string& sfn)  ;
    void    updateReplica(const Replica& replica)  ;

    void utime(ino_t inode, const struct utimbuf* buf)  ;

    void setMode(ino_t inode, uid_t uid, gid_t gid,
                mode_t mode, const Acl& acl)  ;

    void setSize    (ino_t inode, size_t size)  ;
    void setChecksum(ino_t, const std::string&, const std::string&)  ;
    
    std::string getComment   (ino_t inode)  ;
    void        setComment   (ino_t inode, const std::string& comment)  ;
    void        deleteComment(ino_t inode)  ;

    void setGuid(ino_t inode, const std::string& guid)  ;
    
    void updateExtendedAttributes(ino_t inode,
                                  const Extensible& attr)  ;

    IDirectory*    openDir (ino_t inode)  ;
    void           closeDir(IDirectory* dir)  ;  
    ExtendedStat*  readDirx(IDirectory* dir)  ;
    struct dirent* readDir (IDirectory* dir)  ;

   protected:
    void setDpnsApiIdentity();

    StackInstance* si_;

    unsigned    retryLimit_;
    std::string dpnsHost_;

    // Need to keep this in memory, as dpns/dpm API do not make
    // copy of them (sigh)
    // setAuthorizationId does, though (don't ask me why)
    char   **fqans_;
    size_t   nFqans_;

    // Host DN is root
    bool hostDnIsRoot_;
    std::string hostDn_;

    const SecurityContext* secCtx_;
  };

};

#endif	// NS_ADAPTER_H
