/// @file   DomeAdapterAuthn.h
/// @brief  Dome adapter
/// @author Georgios Bitzes <georgios.bitzes@cern.ch>

#ifndef DOME_ADAPTER_AUTHN_H
#define DOME_ADAPTER_AUTHN_H

#include <dmlite/cpp/catalog.h>
#include <dmlite/cpp/dmlite.h>
#include "DomeAdapter.h"
#include "utils/DomeTalker.h"
#include "DomeAdapterIdMapCache.hh"

namespace dmlite {

  extern Logger::bitmask domeadapterlogmask;
  extern Logger::component domeadapterlogname;

  class DomeAdapterAuthn : public Authn {
  public:
  	/// Constructor
    DomeAdapterAuthn(DomeAdapterFactory *factory);

    // Overload
    std::string getImplId(void) const throw ();

    SecurityContext* createSecurityContext(const SecurityCredentials& cred)  ;
    SecurityContext* createSecurityContext()  ;

    GroupInfo newGroup   (const std::string& gname)  ;
    GroupInfo getGroup   (const std::string& groupName)  ;
    GroupInfo getGroup   (const std::string& key,
                          const boost::any& value)  ;
    void      updateGroup(const GroupInfo& group)  ;
    void      deleteGroup(const std::string& groupName)  ;

    UserInfo newUser   (const std::string& uname)  ;
    UserInfo getUser   (const std::string& userName)  ;
    UserInfo getUser   (const std::string& key,
                        const boost::any& value)  ;
    void     updateUser(const UserInfo& user)  ;
    void     deleteUser(const std::string& userName)  ;

    std::vector<GroupInfo> getGroups(void)  ;
    std::vector<UserInfo>  getUsers (void)  ;

    void getIdMap(const std::string& userName,
                  const std::vector<std::string>& groupNames,
                  UserInfo* user,
                  std::vector<GroupInfo>* groups)  ;

  protected:
    void uncachedGetIdMap(const std::string& userName,
                  const std::vector<std::string>& groupNames,
                  UserInfo* user,
                  std::vector<GroupInfo>* groups)  ;

    static IdMapCache idmapCache;

    StackInstance* si_;
    const DomeCredentials emptycreds;
    DomeAdapterFactory* factory_;
  };
}


#endif
