/*
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * Author: Fabrizio Furano, David Smith <David.Smith@cern.ch>
*/

#include <XrdDPMRedirAcc.hh>
#include <XrdDPMTrace.hh>
#include <XrdOuc/XrdOucEnv.hh>
#include <XrdOuc/XrdOucString.hh>
#include <XrdSys/XrdSysPthread.hh>
#include <XrdSec/XrdSecEntity.hh>
#include <XrdSys/XrdSysPlugin.hh>
#include <XrdOuc/XrdOucPinPath.hh>
#include <XrdDPMCommon.hh>
#include <XrdVersion.hh>

#include <vector>
#include <exception>

// globals

XrdVERSIONINFO(XrdAccAuthorizeObject,XrdDPMRedirAcc)
XrdVERSIONINFO(DpmXrdAccAuthorizeObject,XrdDPMRedirAcc)

namespace DpmRedirAcc {
   XrdSysError   Say(0,"dpmrediracc_");
   XrdOucTrace   Trace(&Say);
   XrdAccAuthorize*   tokAuthorization(0);
}

using namespace DpmRedirAcc;

static DpmRedirConfigOptions *RedirConfig = 0;

/******************************************************************************/
/*                       X r d D P M R e d i r A c c                          */
/******************************************************************************/

XrdDPMRedirAcc::XrdDPMRedirAcc(const char *cfn,int ForceSecondary) :
      ForceSecondary(ForceSecondary) {
   if (DpmCommonConfigProc(Say, cfn, CommonConfig)) {
      throw dmlite::DmException(DMLITE_CFGERR(EINVAL),
                                "problem with (common) configuration");
   }
   Trace.What = CommonConfig.OfsTraceLevel;
}

XrdDPMRedirAcc::~XrdDPMRedirAcc() {}

XrdAccPrivs XrdDPMRedirAcc::Access(const XrdSecEntity *Entity,
      const char *path,
      const Access_Operation oper,
      XrdOucEnv *Env)
{
   EPNAME("Access");

   if (!RedirConfig) {
      Say.Emsg("Access","Common redirector configuration not found");
      return XrdAccPriv_None;
   }

   if (!ForceSecondary && oper == AOP_Stat) {
      // may be a discovery stat triggered by a meta-manager.
      // If it's a normal stat the veto & secondary will be
      // called from DPMFinder Locate
      DEBUG("Passing stat directly");
      return XrdAccPriv_Lookup;
   }

   if (ForceSecondary || DpmIdentity::usesPresetID(Env, Entity)) {
      DEBUG("Should use fixed id, proto=" <<
                (Entity ? Entity->prot : "[none]"));
      if (tokAuthorization == 0) {
         TRACE(MOST,"Use of fixed id needs a secondary authorization "
                    "library to be configured. Denying");
         return XrdAccPriv_None;
      }
      XrdAccPrivs ret_privs = tokAuthorization->Access(Entity, path, oper, Env);
      if (ret_privs == XrdAccPriv_None) {
         return ret_privs;
      }
      std::vector<XrdOucString> fp;
      try {
         fp = TranslatePathVec(*RedirConfig, path);
         for(size_t i=0;i<fp.size();++i) {
            fp[i] = CanonicalisePath(SafeCStr(fp[i]), 1);
         }
      } catch (dmlite::DmException &e) {
         XrdOucString err = "Name error: ";
         err += XrdOucString(path)+" ("+DmExStrerror(e)+")";
         DEBUG(err);
         return XrdAccPriv_None;
      } catch(const std::exception &) {
         Say.Emsg("Access","Unexpected exception");
         return XrdAccPriv_None;
      }
      size_t matched = 0;
      for(size_t i=0;i<fp.size();++i) {
         bool ok=0;
         std::vector<XrdOucString>::const_iterator itr;
         for(itr=RedirConfig->AuthLibRestrict.begin();
               itr != RedirConfig->AuthLibRestrict.end(); ++itr) {
            if (fp[i].find(*itr) == 0) {
               ok = 1;
               break;
            }
         }
         if (ok) {
            ++matched;
         }
      }
      // require that all paths returned by the translation are included
      // in the AuthLibRestrict list
      if (matched==0 || matched != fp.size()) {
         TRACE(MOST,"Path vetoed, not in fixed id restrict list");
         return XrdAccPriv_None;
      }
      return ret_privs;
   }

   DEBUG("Passing for pure dpm authorization, "
      "proto=" << (Entity ? Entity->prot : "[none]"));
   
   return XrdAccPriv_All;
}

int XrdDPMRedirAcc::Test(const XrdAccPrivs priv,const Access_Operation oper)
{
   // Warning! This table must be in 1-to-1 correspondence with Access_Operation
   //
   static XrdAccPrivs need[] = {XrdAccPriv_None,                 // 0
            XrdAccPriv_Chmod,                // 1
            XrdAccPriv_Chown,                // 2
            XrdAccPriv_Create,               // 3
            XrdAccPriv_Delete,               // 4
            XrdAccPriv_Insert,               // 5
            XrdAccPriv_Lock,                 // 6
            XrdAccPriv_Mkdir,                // 7
            XrdAccPriv_Read,                 // 8
            XrdAccPriv_Readdir,              // 9
            XrdAccPriv_Rename,               // 10
            XrdAccPriv_Lookup,               // 11
            XrdAccPriv_Update                // 12
                               
   };
   if (oper < 0 || oper > AOP_LastOp) return 0;
   return (int)(need[oper] & priv) == need[oper];
}

int XrdDPMRedirAcc::Audit(const int accok,
      const XrdSecEntity *Entity,
      const char *path,
      const Access_Operation oper,
      XrdOucEnv *Env)
{
   return accok;
}

extern "C"
{

XrdAccAuthorize *DpmXrdAccAuthorizeObject(XrdSysLogger *lp,
               const char   *cfn,
               const char   *parm,
               int ForceSecondary,
               DpmRedirConfigOptions *rconf)
{
   //EPNAME("DpmXrdAccAuthorizeObject");
   typedef XrdAccAuthorize* (*ept)(XrdSysLogger *, const char *, const char *);
   static bool initialised = 0;
   XrdSysPlugin *tokLib;

   if (!RedirConfig)
      RedirConfig = rconf;

   if (!initialised) {
      initialised = 1;

      Say.logger(lp);
      XrdSysError_Table *ETab = XrdDmliteError_Table();
      Say.addTable(ETab);

      XrdDmCommonInit(lp);

      XrdOucString mypar(parm),AuthLib,Args;
      int from = 0;
      from = mypar.tokenize(AuthLib, from, ' ');
      if (from != STR_NPOS) { Args.assign(mypar, from, -1); }

      if (AuthLib.length()) {
         char libBuf[2048];
         bool noFallBack;
         char *theLib,*altLib;
         if (!XrdOucPinPath(SafeCStr(AuthLib),
             noFallBack, libBuf,sizeof(libBuf))) {
            theLib = strdup(SafeCStr(AuthLib));
            altLib = 0;
         } else {
            theLib = strdup(libBuf);
            altLib = (noFallBack ? 0 : strdup(SafeCStr(AuthLib)));
         }
         ept ep = 0;
         tokLib = new XrdSysPlugin(&Say, theLib);
         if (tokLib) {
            ep = reinterpret_cast<ept>(reinterpret_cast<size_t>
               (tokLib->getPlugin("XrdAccAuthorizeObject")));
         }
         if (!ep && altLib) {
            delete tokLib;
            tokLib = new XrdSysPlugin(&Say, altLib);
            if (tokLib) {
               ep = reinterpret_cast<ept>(reinterpret_cast<size_t>
                  (tokLib->getPlugin("XrdAccAuthorizeObject")));
            }
         }
         free(theLib);
         free(altLib);

         if (!tokLib) {
            DpmRedirAcc::Say.Emsg("NewObject","Could not open authorization library",
                     SafeCStr(AuthLib));
            return 0;
         }

         if (!ep || (tokAuthorization =
               ep(lp, cfn, Args.length() ? Args.c_str() : 0)) == 0) {
            Say.Emsg("NewObject", "Could not get an authorization "
               "instance from libary", SafeCStr(AuthLib));
            delete tokLib;
            return 0;
         }
      }
   }

   if (ForceSecondary && !tokAuthorization) { return 0; }

   try {
      return new XrdDPMRedirAcc(cfn,ForceSecondary);
   } catch (dmlite::DmException &e) {
     Say.Emsg("NewObject","cannot start the access control layer", e.what());
   } catch(const std::exception &) {
     Say.Emsg("NewObject","unexpected exception");
   }
   return 0;
}
} // extern "C"

extern "C"
{
XrdAccAuthorize *XrdAccAuthorizeObject(XrdSysLogger *lp,
                                       const char   *cfn,
                                       const char   *parm)
{
   //EPNAME("XrdAccAuthorizeObject");

   return DpmXrdAccAuthorizeObject(
      lp,cfn,parm,0,(DpmRedirConfigOptions *)0);
}
} // extern "C"
