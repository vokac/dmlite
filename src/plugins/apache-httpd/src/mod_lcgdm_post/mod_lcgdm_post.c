/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <httpd.h>
#include <http_config.h>
#include <http_request.h>
#include "mod_lcgdm_post.h"
#include "form_helper.h"

static const char* post_get_boundary(request_rec *r)
{
    const char* content_type = apr_table_get(r->headers_in, "Content-Type");
    const char* boundary = NULL;

    if (content_type
            && strncasecmp(content_type, "multipart/form-data; boundary=", 30)
                    == 0) {
        boundary = content_type + 30;
    }

    return boundary;
}

static int post_request_is_dir(request_rec *r)
{
    return r->parsed_uri.path[strlen(r->parsed_uri.path) - 1] == '/';
}

static void post_patch_request(request_rec *r, const char *fname,
        const char *boundary)
{
    r->method = "PUT";
    r->method_number = M_PUT;
    if (post_request_is_dir(r)) {
        r->uri = apr_pstrcat(r->pool, r->uri, fname, NULL );
        r->parsed_uri.path = apr_pstrcat(r->pool, r->parsed_uri.path, fname,
                NULL );
    }
    ap_add_input_filter("post2put-filter", form_filter_new_context(r, boundary),
            r, r->connection);
}

static int post_fixups(request_rec *r)
{
    post_dir_conf *conf;
    conf = ap_get_module_config(r->per_dir_config, &lcgdm_post_module) ;

    // Post2Put enabled and method is POST
    if (!conf->field || r->method_number != M_POST)
        return DECLINED;

    // Get the boundary
    const char* boundary = post_get_boundary(r);
    if (!boundary)
        return HTTP_BAD_REQUEST;

    // Iterate the form fields
    const char *filename = NULL;
    const char *fieldname;
    apr_table_t *fieldattrs = apr_table_make(r->pool, 10);

    while ((fieldname = post_form_next(r, boundary, fieldattrs))) {

        // This is the one!
        if (strcmp(fieldname, conf->field) == 0) {
            const char *type = apr_table_get(fieldattrs, "Content-Type");
            if (!type || strcasecmp(type, "multipart/") != 0) {
                filename = post_form_filename(r->pool, fieldattrs);
                break;
            }
        }

        // Do not accept any other field
        else {
            return HTTP_BAD_REQUEST;
        }

        apr_table_clear(fieldattrs);
    }

    // Patch the request and keep going
    if (filename) {
        post_patch_request(r, filename, boundary);
        return OK;
    }
    else {
        return HTTP_BAD_REQUEST;
    }
}

static void register_hooks(apr_pool_t *p)
{
    ap_hook_fixups(post_fixups, NULL, NULL, APR_HOOK_FIRST);
    ap_register_input_filter("post2put-filter", form_filter, NULL,
            AP_FTYPE_RESOURCE);
}

static void *post_create_dir_config(apr_pool_t *p, char *dir)
{
    post_dir_conf* conf = apr_pcalloc(p, sizeof(*conf));
    return conf;
}

static const char *post_cmd_post2put(cmd_parms *cmd, void *config,
        const char *arg1)
{
    post_dir_conf* conf = config;
    conf->field = arg1;
    return NULL ;
}

/* Module configuration */
static const command_rec post_cmds[] =
        {
                        AP_INIT_TAKE1("Post2Put", post_cmd_post2put, NULL,
                                ACCESS_CONF, "specify the form field name that contains the file"),
                {
                        NULL } };

/* Module */
module lcgdm_post_module = {
        STANDARD20_MODULE_STUFF, post_create_dir_config, NULL, NULL, NULL,
        post_cmds, register_hooks, };
