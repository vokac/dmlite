/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SECURITY_H_
#define	SECURITY_H_

#include <dmlite/c/dmlite.h>
#include <httpd.h>
#include <openssl/ssl.h>

#define DAV_SHARED_MAX_FQANS       32
#define DAV_SHARED_MAX_DELEGATION 512

/**
 * Interacts with mod_gridsite through environment variables and initializes
 * an instance of dav_shared_user_info.
 * @param pool    The pool used to allocate the needed data.
 * @param r       The request.
 * @param anon    The anonymous user, if any.
 * @param trusted The trusted DNs, if any.
 * @return        A pointer to an initialized struct credentials.
 *                NULL on error.
 */
dmlite_credentials *dav_shared_get_user_credentials(apr_pool_t *pool,
        request_rec *r, const char *anon_usr, const char *anon_grp,
        apr_array_header_t* trusted);

/**
 * Returns 1 if ssl is enabled in this request
 */
char is_ssl_used(request_rec *r);


int dav_shared_pass_query(void *rec, const char *key, const char *value);
void dav_shared_pass_query_args(apr_pool_t *pool, dmlite_credentials *creds, request_rec *r);
int dav_shared_pass_header(void *rec, const char *key, const char *value);
void dav_shared_pass_headers(apr_pool_t *pool, dmlite_credentials *creds, request_rec *r);


#endif	/* SECURITY_H_ */
