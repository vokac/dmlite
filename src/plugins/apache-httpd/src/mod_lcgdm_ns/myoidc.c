
/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>

#define __USE_XOPEN
#include <time.h>

#include <apr_file_io.h>
#include "c/dmlite.h"
#include "c/catalog.h"
#include "c/pool.h"

#include <httpd.h>
#include <http_protocol.h>
#include <mod_dav.h>
#include <apr_strings.h>
#include <http_log.h>
#include "mod_lcgdm_ns.h"
#include "myoidc.h"
#include <httpd.h>

int dav_ns_init_info_from_oidc(dav_error **reterr, dav_resource_private *info) {
  dmlite_credentials *creds;
  const char *s;
  request_rec *r = info->request;

  /* Create user information */
  creds = apr_pcalloc(r->pool, sizeof(dmlite_credentials));
  
  // Look for an OIDC var tht's supposed to be always there in case of OIDC
  s = apr_table_get(r->headers_in, "OIDC_CLAIM_sub");
  if (s) {
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                  "Detected OIDC information. Sub: '%s'", s);
    creds->client_name = apr_pstrdup(r->pool, s);
  }
  else return 1; // No bearer token info could be found
  
  
  creds->extra = dmlite_any_dict_new();
  creds->mech = "OIDC";
  
  /* Client IP */
  #if AP_SERVER_MAJORVERSION_NUMBER == 2 &&\
  AP_SERVER_MINORVERSION_NUMBER < 4
  creds->remote_address = apr_pstrdup(r->pool, r->connection->remote_ip);
  #else
  creds->remote_address = apr_pstrdup(r->pool, r->useragent_ip);
  #endif
  
  /* Pass query and headers */
  //dav_shared_pass_query_args(pool, creds, r);
  //dav_shared_pass_headers(pool, creds, r);
  
  // Pass selected fields coming from OIDC
  creds->oidc_audience = apr_table_get(r->headers_in, "OIDC_CLAIM_aud");
  creds->oidc_scope = apr_table_get(r->headers_in, "OIDC_CLAIM_scope");
  creds->oidc_issuer = apr_table_get(r->headers_in, "OIDC_CLAIM_iss");
  
  
  
  // Now see if there are groups to be fetched as fqans      
  creds->nfqans = 0;
        
  // Now try to get the wlcg groups (if any) from the OIDC info
  // these are optional
  s = apr_table_get(r->headers_in, "OIDC_CLAIM_wlcg.groups");
  if (s) {
    creds->fqans = apr_pcalloc(r->pool, sizeof(char*) * DAV_SHARED_MAX_FQANS);
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                  "Adding wlcg groups '%s'", s);
    
    // Now parse this comma-delimited list. These are going to be the groups of this user
    char *saveptr;
    char *c = strtok_r((char *)s, ",", &saveptr);
    while ( c && (creds->nfqans < DAV_SHARED_MAX_FQANS)) {
      
      // Skip the leading slash in the group names coming from OIDC
      if (*c == '/')
        creds->fqans[creds->nfqans++] = apr_pstrdup(r->pool, c+1);
      else
        creds->fqans[creds->nfqans++] = apr_pstrdup(r->pool, c);
      
      c = strtok_r(NULL, ",", &saveptr);
    }
    
    
  }

  /* Done here */
  unsigned i;
  ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, r, "Using client name: '%s' scope: '%s' issuer: '%s'", creds->client_name, creds->oidc_scope, creds->oidc_issuer);
  
  for (i = 0; i < creds->nfqans; ++i)
    ap_log_rerror(APLOG_MARK, APLOG_INFO, 0, r, "Using group: '%s'", creds->fqans[i]);
  
  info->user_creds = creds;
  return 0;
}
