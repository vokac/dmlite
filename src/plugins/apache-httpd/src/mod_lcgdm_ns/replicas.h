/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef REPLICAS_H_
#define REPLICAS_H_

#include <apr_pools.h>
#include <dmlite/c/inode.h>
#include "mod_lcgdm_ns.h"

struct dav_ns_replica_array
{
    int nreplicas;
    dmlite_replica *replicas;
    char *action;
};

/**
 * Serialize a set of replicas to JSON.
 * @param req       The request.
 * @param nreplicas The number of replicas in the array.
 * @param replicas  An array of nreplicas replicas.
 * @return          The serialized version.
 */
char* dav_ns_serialize_replicas(request_rec *req, int nreplicas,
        dmlite_replica *replicas);
/**
 * Deserialize a set of replicas from JSON.
 * @param req       The request.
 * @param json_str  The JSON representation.
 * @param replicas  It has to point to a struct dav_ns_replica array, which will be initialized.
 * @return          NULL on success.
 */
dav_error* dav_ns_deserialize_replicas(request_rec *req, const char* json_str,
        struct dav_ns_replica_array* replicas);

#endif /* REPLICAS_H_ */
