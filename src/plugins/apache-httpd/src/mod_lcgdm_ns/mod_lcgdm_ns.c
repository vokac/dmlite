/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <ctype.h>
#include <httpd.h>
#include <http_connection.h>
#include <http_log.h>
#include <pthread.h>
#include "../client/htext.h"

#include "mime.h"
#include "mod_lcgdm_ns.h"

/** Provider definition */
static const dav_provider dav_ns_provider = {
    &dav_ns_hooks_repository, /* Repository handlers  */
    &dav_ns_hooks_db, /* DB Handlers          */
    NULL, /* Lock                 */
    NULL, /* Versioning           */
    NULL, /* Binding              */
    NULL, /* Search               */
    NULL /* Context              */
};

/** Correspondence string->flag */
typedef struct
{
    const char *str;
    dir_flags flag;
} dav_ns_dir_flags;

/** Mapping from string to integer (flags) */
static dav_ns_dir_flags dav_ns_dir_flags_known[] = {
    {"write", DAV_NS_WRITE },
    {"noauthn", DAV_NS_NOAUTHN },
    {"remotecopy", DAV_NS_REMOTE_COPY },
    {"remotecopydmlite", DAV_NS_REMOTE_COPY | DAV_NS_REMOTE_COPY_DMLITE},
    {NULL, 0 }
};

/**
 * Creates a structure for the server configuration
 * @param p The pool used to allocate memory
 * @param s Information about the virtual server being configured
 * @return A clean dav_ns_server_conf instance
 */
static void *dav_ns_create_server_config(apr_pool_t *p, server_rec *s)
{
    (void) s;
    dav_ns_server_conf *conf = apr_pcalloc(p, sizeof(dav_ns_server_conf));

    /* Set defaults */
    conf->type = DAV_NS_NODE_HEAD;

    return conf;
}

/**
 * Creates a structure for the directory configuration
 * @param p   The pool used to allocate memoryu
 * @param dir The directory being cofigured
 * @return A clean dav_ns_dir_conf instance
 */
static void *dav_ns_create_dir_config(apr_pool_t *p, char *dir)
{
    (void) dir;

    dav_ns_dir_conf *conf = apr_pcalloc(p, sizeof(dav_ns_dir_conf));

    /* Set defaults */
    conf->flags = 0x00;
    conf->trusted_dns = NULL;
    conf->redirect.scheme = "https";
    conf->redirect.port_unsecure = 80;
    conf->redirect.port_secure = 443;
    conf->macaroon_secret = NULL;
    conf->macaroon_secret_size = 0;

    /* Create a context key for each directory */
    conf->context_key = apr_pstrcat(p, "nscontext_", dir, NULL);

    return conf;
}

/**
 * Function called for the configuration parameter NSHost
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config Config pointer
 * @param arg    The value of the parameter
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_ns_cmd_dmlite(cmd_parms *cmd, void *config,
        const char *arg)
{
    (void) config;

    if (cmd->path == NULL) {
        dav_ns_server_conf *conf;
        conf = ap_get_module_config(cmd->server->module_config, &lcgdm_ns_module) ;

        conf->manager = dmlite_manager_new();
        apr_pool_pre_cleanup_register(cmd->pool, conf->manager,
                (apr_status_t (*)(void*)) dmlite_manager_free);

        if (dmlite_manager_load_configuration(conf->manager, arg) != 0)
            return apr_psprintf(cmd->pool, "Could not load %s (%s)", arg,
                    dmlite_manager_error(conf->manager));
    }
    else {
        dav_ns_dir_conf *conf = config;

        conf->manager = dmlite_manager_new();
        apr_pool_pre_cleanup_register(cmd->pool, conf->manager,
                (apr_status_t (*)(void*)) dmlite_manager_free);

        if (dmlite_manager_load_configuration(conf->manager, arg) != 0)
            return apr_psprintf(cmd->pool, "Could not load directory dmlite %s (%s)", arg,
                    dmlite_manager_error(conf->manager));
    }

    return NULL ;
}

/**
 * Function called for the configuration parameter NodeType
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config Unused pointer
 * @param arg    The value of the parameter
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_ns_cmd_node_type(cmd_parms *cmd, void *config,
        const char *arg)
{
    (void) config;

    dav_ns_server_conf *conf;
    conf = ap_get_module_config(cmd->server->module_config, &lcgdm_ns_module) ;
    if (strcasecmp(arg, "head") == 0 || strcasecmp(arg, "dpm") == 0)
        conf->type = DAV_NS_NODE_HEAD;
    else if (strcasecmp(arg, "lfc") == 0)
        conf->type = DAV_NS_NODE_LFC;
    else if (strcasecmp(arg, "plain") == 0)
        conf->type = DAV_NS_NODE_PLAIN;
    else
        return "Unknown value for NodeType";
    return NULL ;
}

/**
 * Set the flags for a directory/location. Called once per flag.
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param w      The flag
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_ns_cmd_flags(cmd_parms *cmd, void *config, const char *w)
{
    dav_ns_dir_conf *conf = (dav_ns_dir_conf*) config;
    dav_ns_dir_flags *iter;

    for (iter = dav_ns_dir_flags_known; iter->str; ++iter) {
        if (strcasecmp(w, iter->str) == 0) {
            conf->flags |= iter->flag;

            if ((conf->flags & DAV_NS_NOAUTHN)
                    && (conf->flags & DAV_NS_WRITE)) {
                ap_log_error(APLOG_MARK, APLOG_WARNING, 0, cmd->server,
                        "You are disabling authentication and allowing write mode!");
                ap_log_error(APLOG_MARK, APLOG_WARNING, 0, cmd->server,
                        "This is probably not what you want");
                ap_log_error(APLOG_MARK, APLOG_WARNING, 0, cmd->server,
                        "If that's the case, please, check NSFlags value");
            }

            return NULL ;
        }
    }

    return apr_psprintf(cmd->pool, "%s is not a recognised flag", w);
}

/**
 * Set the "anonymous" user
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param arg    The user
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_ns_cmd_anon(cmd_parms *cmd, void *config,
        const char *arg)
{
    dav_ns_dir_conf *conf = (dav_ns_dir_conf*) config;
    const char *colon = strchr(arg, ':');

    if (!colon) {
        conf->anon_group = conf->anon_user = apr_pstrdup(cmd->pool, arg);
    }
    else {
        size_t ulen = colon - arg;
        conf->anon_user = apr_pcalloc(cmd->pool, ulen + 1); // Remember \0!
        memcpy(conf->anon_user, arg, ulen);

        conf->anon_group = apr_pstrdup(cmd->pool, colon + 1);
    }
    return NULL ;
}

/**
 * Sets how many replicas will be pushed into a URL on LFC's
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param arg    A string that must be an integer
 * @return
 */
static const char *dav_ns_cmd_replicas(cmd_parms *cmd, void *config,
        const char *arg)
{
    (void) cmd;

    int i, len;

    dav_ns_dir_conf *conf = (dav_ns_dir_conf*) config;

    len = strlen(arg);
    for (i = 0; i < len; ++i) {
        if (!isdigit(arg[i]))
            return "MaxReplicas only accepts unsigned integers as a parameter";
    }

    conf->max_replicas = atoi(arg);

    return NULL ;
}

/**
 * Sets which list of DN's are trusted to give directly the user DN, FQAN, ...
 * @param cmd    Lots of information about the configuration contexts (as pool).
 * @param config A pointer to the directory configuration.
 * @param w      The trusted DN.
 * @return       NULL on success. An error string otherwise.
 */
static const char *dav_ns_cmd_trusted(cmd_parms *cmd, void *config,
        const char *w)
{
    dav_ns_dir_conf *conf = (dav_ns_dir_conf*) config;

    if (conf->trusted_dns == NULL )
        conf->trusted_dns = apr_array_make(cmd->pool, 5, sizeof(char*));

    char **dn = apr_array_push(conf->trusted_dns);
    *dn = apr_pstrdup(cmd->pool, w);

    return NULL ;
}

/**
 *
 * @param cmd    Lots of information about the configuration contexts (as pool).
 * @param config A pointer to the directory configuration.
 * @param arg    0 if the flag is set to Off
 * @return
 */
static const char *dav_ns_secureredir(cmd_parms *cmd, void *config, int arg)
{
    (void) cmd;

    dav_ns_dir_conf *conf = (dav_ns_dir_conf*) config;

    if (arg)
        conf->redirect.scheme = "https";
    else
        conf->redirect.scheme = "http";

    return NULL ;
}

/**
 * Set the default redirection ports
 * @param cmd      Lots of information about the configuration contexts (as pool)
 * @param config   A pointer to the directory configuration
 * @param unsecure The unsecure port (as a string)
 * @param secure   The secure port (as a string)
 * @return         NULL on success. An error string otherwise
 */
static const char *dav_ns_redirect_port(cmd_parms *cmd, void *config,
        const char *unsecure, const char *secure)
{
    dav_ns_dir_conf *conf = (dav_ns_dir_conf*) config;
    conf->redirect.port_unsecure = atoi(unsecure);
    conf->redirect.port_secure = atoi(secure);
    return NULL ;
}

/**
 * Set the macaroon secret
 * @param cmd      Lots of information about the configuration contexts (as pool)
 * @param config   A pointer to the directory configuration
 * @param arg      The secret
 * @return         NULL on success. An error string otherwise
 */
static const char *dav_ns_macaroon_secret(cmd_parms *cmd, void *config,
    const char *arg)
{
    dav_ns_dir_conf *conf = (dav_ns_dir_conf*) config;
    conf->macaroon_secret = apr_pstrdup(cmd->pool, arg);
    conf->macaroon_secret_size = strlen(conf->macaroon_secret);
    return NULL;
}




/**
 * Set the proxy cache location
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param arg    The user
 * @return       NULL on success. An error string otherwise
 */
static const char *dav_ns_cmd_proxy_cache(cmd_parms *cmd, void *config,
                                            const char *arg)
{
  (void) cmd;
  
  dav_ns_dir_conf *conf = (dav_ns_dir_conf*) config;
  conf->proxy_cache = arg;
  return NULL ;
}

/**
 * Sets the delegation service URL.
 * @param cmd    Lots of information about the configuration contexts (as pool)
 * @param config A pointer to the directory configuration
 * @param arg    The URL or path.
 * @return
 */
static const char *dav_ns_cmd_delegation(cmd_parms *cmd, void *config,
                                           const char *arg)
{
  (void) cmd;
  
  dav_ns_dir_conf *conf = (dav_ns_dir_conf*) config;
  conf->delegation_service = arg;
  return NULL ;
}









/** Command list (configuration parameters) */
static const command_rec dav_ns_cmds[] =
{
    AP_INIT_TAKE1 ("NSDMLite", dav_ns_cmd_dmlite, NULL, RSRC_CONF | ACCESS_CONF,
            "DMLite configuration file"),
    AP_INIT_TAKE1 ("NSType", dav_ns_cmd_node_type, NULL, RSRC_CONF,
            "Node type"),
    AP_INIT_ITERATE("NSFlags", dav_ns_cmd_flags, NULL, ACCESS_CONF,
            "Directory flags"),
    AP_INIT_TAKE1 ("NSAnon", dav_ns_cmd_anon, NULL, ACCESS_CONF,
            "Anonymous user for fallback"),
    AP_INIT_TAKE1 ("NSMaxReplicas", dav_ns_cmd_replicas, NULL, ACCESS_CONF,
            "Maximum number of simultaneous replicas to push into the redirect"),
    AP_INIT_ITERATE("NSTrustedDNS", dav_ns_cmd_trusted, NULL, ACCESS_CONF,
            "List of trusted DN"),
    AP_INIT_FLAG ("NSSecureRedirect", dav_ns_secureredir, NULL, ACCESS_CONF,
            "If On, https will be used. If Off, http."),
    AP_INIT_TAKE2 ("NSRedirectPort", dav_ns_redirect_port, NULL, ACCESS_CONF,
            "Default ports to use on redirection if a replica does not specify it (unsecure secure)"),
    AP_INIT_TAKE1 ("NSMacaroonSecret", dav_ns_macaroon_secret, NULL, ACCESS_CONF,
            "Macaroon secret"),
    AP_INIT_TAKE1 ("NsProxyCache", dav_ns_cmd_proxy_cache, NULL, ACCESS_CONF,
            "Proxy cache location"),
    AP_INIT_TAKE1 ("NsProxyDelegationService", dav_ns_cmd_delegation, NULL, ACCESS_CONF,
            "Delegation service path or URL"), {NULL, {NULL}, NULL, 0, 0, NULL}
};

/**
 * Called to initialize each children
 * @param pool
 * @param server
 */
static void dav_ns_child_init(apr_pool_t *pool, server_rec *server)
{
    (void) pool;
    (void) server;
    /* Nothing */
}

/**
 * Callback used to register the hooks.
 * This is the entry point of the module, where all hooks are registered in
 * mod_dav so the requests can be passed.
 * @param p A pool to use for allocations
 */
static void mod_lcgdm_ns_register_hooks(apr_pool_t *p)
{
    dav_ns_mime_init(p, "/etc/mime.types");

    dav_hook_find_liveprop(dav_ns_find_liveprop, NULL, NULL, APR_HOOK_MIDDLE);
    dav_hook_insert_all_liveprops(dav_ns_insert_all_liveprops, NULL, NULL,
            APR_HOOK_MIDDLE);
    dav_register_liveprop_group(p, &dav_ns_liveprop_group);

    dav_register_provider(p, "nameserver", &dav_ns_provider);

    ap_hook_child_init(dav_ns_child_init, NULL, NULL, APR_HOOK_MIDDLE);
}

/**
 * Module's data structure.
 * This is the joint between the module and Apache (callbacks)
 * 'module' is defined in httpd.h
 */
module AP_MODULE_DECLARE_DATA lcgdm_ns_module = {
        STANDARD20_MODULE_STUFF,
        dav_ns_create_dir_config,    /* Per-directory config creator */
        NULL,                        /* Dir config merger            */
        dav_ns_create_server_config, /* Server config creator        */
        NULL,                        /* Server config merger         */
        dav_ns_cmds,                 /* Command table                */
        mod_lcgdm_ns_register_hooks  /* Register hooks               */
};
