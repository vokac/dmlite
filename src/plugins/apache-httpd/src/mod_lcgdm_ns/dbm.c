/**
 * @file   dbm.c
 * @brief  Dead-properties. Not supported.
 * @author Alejandro Álvarez Ayllón <aalvarez@cern.ch>
 */
#include <apr_strings.h>
#include <dmlite/c/any.h>
#include <dmlite/c/catalog.h>
#include <httpd.h>
#include <http_log.h>

#include "mod_lcgdm_ns.h"
#include "../shared/utils.h"

/** Internal structure for property DB handling */
struct dav_db
{
    /* Context */
    apr_pool_t *pool;
    request_rec *request;
    dav_resource_private *info;
    dmlite_any_dict *xattr;
    char xattr_modified;
    /* Read-only? */
    int ro;
    /* Used to iterate by dav_dpm_propdb_*name */
    unsigned i;
    /* Keys on the dict */
    unsigned n_keys;
    char **keys;
    dav_prop_name *properties;
    char prefixes_initialized;
};

/** Internal structure for rollback */
struct dav_deadprop_rollback
{
    dmlite_any_dict *xattr;
};

/**
 * Open the properties DB
 * @param p The pool used to allocate memory
 * @param resource The resource being processed
 * @param ro       Read-only mode (1)
 * @param pdb      Here we put the created dav_db
 * @return         NULL on success
 */
static dav_error *dav_dpm_propdb_open(apr_pool_t *p,
        const dav_resource *resource, int ro, dav_db **pdb)
{
    dav_db *db;
    unsigned i;

    /* Initialize */
    db = apr_pcalloc(p, sizeof(dav_db));
    apr_pool_create(&db->pool, p);
    db->info = resource->info;
    db->request = resource->info->request;
    db->xattr = resource->info->stat.extra;
    db->xattr_modified = 0;
    db->ro = ro;
    db->properties = NULL;
    db->prefixes_initialized = 0;

    *pdb = db;

    dmlite_any_dict_keys(resource->info->stat.extra, &db->n_keys, &db->keys);
    db->properties = apr_pcalloc(db->pool, sizeof(dav_prop_name) * db->n_keys);

    for (i = 0; i < db->n_keys; ++i) {
        char *key = db->keys[i];
        char *sep = strchr(key, ' ');

        if (sep == NULL ) {
            db->properties[i].ns = "LCGDM:";
            db->properties[i].name = key;
        }
        else {
            *sep = '\0';
            db->properties[i].ns = key;
            db->properties[i].name = sep + 1;
        }
    }

    return NULL ;
}

/**
 * Closes the property handler
 * @param db The DB to close
 */
static void dav_dpm_propdb_close(dav_db *db)
{
    /* If changed, store */
    if (db->xattr_modified)
        dmlite_update_xattr(db->info->ctx, db->info->sfn, db->xattr);

    /* Clean */

    /* Currently, mod_dav's pool usage doesn't allow clearing this pool.
    ** see same restriction in dav_close_propdb(). e.g. of such usage is use
    ** of results from dav_get_allprops() after calling dav_close_propdb() in
    ** mod_dav's dav_propfind_walker()
    */
#if 0
    apr_pool_destroy(db->pool);
#endif

    dmlite_any_dict_keys_free(db->n_keys, db->keys);
}

/**
 * Define namespaces the values may need
 * @param db
 * @param xi
 * @return
 */
static dav_error *dav_dpm_propdb_define_namespaces(dav_db *db,
        dav_xmlns_info *xi)
{
    unsigned i;
    apr_pool_t *subpool;

    if (db->prefixes_initialized)
        return NULL ;

    /* LCGDM: Always needed */
    apr_hash_set(xi->prefix_uri, "lcgdm", 5, "LCGDM:");
    apr_hash_set(xi->uri_prefix, "LCGDM:", 6, "lcgdm");

    /* Add additional thay may be found */
    apr_pool_create(&subpool, db->pool);
    for (i = 0; i < db->n_keys; ++i) {
        char prefix[8];
        int new_i = xi->count++;
        int psize = snprintf(prefix, sizeof(prefix), "ns%d", new_i);
        int uri_len = strlen(db->properties[i].ns);

        if (apr_hash_get(xi->uri_prefix, db->properties[i].ns,
                uri_len) == NULL) {
            char *ns = apr_pstrdup(subpool, prefix);

            apr_hash_set(xi->prefix_uri, ns, psize, db->properties[i].ns);
            apr_hash_set(xi->uri_prefix, db->properties[i].ns, uri_len, ns);

            apr_pool_clear(subpool);
        }
    }
    apr_pool_destroy(subpool);

    db->prefixes_initialized = 1;

    return NULL ;
}

/**
 * Put the value for the specified property into phdr
 * @param db    The database being examinated
 * @param name  The property name
 * @param xi    Namespace handling
 * @param phdr  Where to attach the value
 * @param found Must be set to 1 if the attribute is found
 * @return      NULL on success
 */
static dav_error *dav_dpm_propdb_output_value(dav_db *db,
        const dav_prop_name *name, dav_xmlns_info *xi, apr_text_header *phdr,
        int *found)
{
    const char *id;
    dmlite_any *any;

    dav_dpm_propdb_define_namespaces(db, xi);

    if (strcmp(name->ns, "LCGDM:") != 0)
        id = apr_pstrcat(db->pool, name->ns, " ", name->name, NULL );
    else
        id = name->name;

    any = dmlite_any_dict_get(db->xattr, id);

    if (any == NULL ) {
        *found = 0;
    }
    else {
        char buffer[512];
        const char *s, *prefix;

        prefix = apr_hash_get(xi->uri_prefix, name->ns, strlen(name->ns));

        dmlite_any_to_string(any, buffer, sizeof(buffer));

        s = apr_psprintf(db->pool, "<%s:%s>%s</%s:%s>", prefix, name->name,
                apr_xml_quote_string(db->pool, buffer, 0), prefix, name->name);

        *found = 1;
        apr_text_append(db->pool, phdr, s);
    }

    dmlite_any_free(any);

    return NULL ;
}

/**
 * Build a mapping from "global" namespaces into provider-local namespace identifiers.
 * This is done before calling store hook
 * @param db
 * @param namespaces
 * @param mapping
 * @return
 */
static dav_error *dav_dpm_propdb_map_namespaces(dav_db *db,
        const apr_array_header_t *namespaces, dav_namespace_map **mapping)
{
    (void) db;
    (void) namespaces;

    *mapping = NULL;
    return NULL ;
}

/**
 * Store a property value
 * @param db
 * @param name
 * @param elem
 * @param mapping
 * @return
 */
static dav_error *dav_dpm_propdb_store(dav_db *db, const dav_prop_name *name,
        const apr_xml_elem *elem, dav_namespace_map *mapping)
{
    (void) mapping;

    const char *data = elem->first_cdata.first->text;
    const char *id;
    dmlite_any* any = dmlite_any_new_string(data);

    if (strcmp(name->ns, "LCGDM:") != 0)
        id = apr_pstrcat(db->pool, name->ns, " ", name->name, NULL );
    else
        id = name->name;

    dmlite_any_dict_insert(db->xattr, id, any);
    dmlite_any_free(any);

    db->xattr_modified = 1;

    return NULL ;
}

/**
 * Remove a property
 * @param db
 * @param name
 * @return
 */
static dav_error *dav_dpm_propdb_remove(dav_db *db, const dav_prop_name *name)
{
    const char *id;

    if (strcmp(name->ns, "LCGDM:") != 0)
        id = apr_pstrcat(db->pool, name->ns, " ", name->name, NULL );
    else
        id = name->name;

    dmlite_any_dict_erase(db->xattr, id);

    db->xattr_modified = 1;

    return NULL ;
}

/**
 * Check if a given property is already defined
 * @param db
 * @param name
 * @return
 */
static int dav_dpm_propdb_exists(dav_db *db, const dav_prop_name *name)
{
    unsigned i;

    for (i = 0; i < db->n_keys; ++i) {
        if (strcmp(db->properties[i].ns, name->ns) == 0
                && strcmp(db->properties[i].name, name->name) == 0)
            return 1;
    }

    return 0;
}

/**
 * Return the first supported property
 * @param db    The active DB instance
 * @param pname Where to put the property
 * @return      NULL on success
 */
static dav_error *dav_dpm_propdb_first_name(dav_db *db, dav_prop_name *pname)
{
    db->i = 0;
    if (db->n_keys > 0) {
        pname->ns = "LCGDM:";
        pname->name = db->keys[0];
    }
    else {
        pname->ns = pname->name = NULL;
    }
    return NULL ;
}

/**
 * Iterates to the next supported property
 * @param db    The active DB instance
 * @param pname Where to put the property
 * @return      NULL on success
 */
static dav_error *dav_dpm_propdb_next_name(dav_db *db, dav_prop_name *pname)
{
    ++db->i;
    if (db->i > db->n_keys) {
        pname->name = db->keys[db->i];
    }
    else {
        pname->ns = pname->name = NULL;
    }
    return NULL ;
}

/**
 * Get rollback context
 * @param db
 * @param name
 * @param prollback
 * @return
 */
static dav_error *dav_dpm_propdb_get_rollback(dav_db *db,
        const dav_prop_name *name, dav_deadprop_rollback **prollback)
{
    (void) name;

    *prollback = apr_pcalloc(db->pool, sizeof(dav_deadprop_rollback));
    (*prollback)->xattr = dmlite_any_dict_copy(db->xattr);
    apr_pool_pre_cleanup_register(db->pool, (*prollback)->xattr,
            (apr_status_t (*)(void*)) dmlite_any_dict_free);
    return NULL ;
}

/**
 * Apply rollback
 * @param db
 * @param rollback
 * @return
 */
static dav_error *dav_dpm_propdb_apply_rollback(dav_db *db,
        dav_deadprop_rollback *rollback)
{
    db->xattr = rollback->xattr;
    db->xattr_modified = 0;
    return NULL ;
}

/** Properties hooks */
const dav_hooks_db dav_ns_hooks_db = {
        dav_dpm_propdb_open, dav_dpm_propdb_close,
        dav_dpm_propdb_define_namespaces, dav_dpm_propdb_output_value,
        dav_dpm_propdb_map_namespaces, dav_dpm_propdb_store,
        dav_dpm_propdb_remove, dav_dpm_propdb_exists, dav_dpm_propdb_first_name,
        dav_dpm_propdb_next_name, dav_dpm_propdb_get_rollback,
        dav_dpm_propdb_apply_rollback, NULL /* Context */
};
