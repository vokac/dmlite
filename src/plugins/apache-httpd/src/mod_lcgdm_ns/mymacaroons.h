
/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOD_LCGDM_NS_MACAROONS_H
#define MOD_LCGDM_NS_MACAROONS_H

#include "../mod_dav/mod_dav.h"

typedef enum {
    ACTIVITY_NONE = 0,
    ACTIVITY_DOWNLOAD = 1 << 0,
    ACTIVITY_UPLOAD   = 1 << 1,
    ACTIVITY_LIST     = 1 << 2,
    ACTIVITY_DELETE   = 1 << 3,
    ACTIVITY_MANAGE   = 1 << 4,
} activity_t;

#define MACAROON_MECH "macaroon"

/**
 * Fill the info from a macaroon
 * @return 0: OK, done and this was a valid macaroon
 *         1: Can't find a macaroon here
 *         >1: there was an invalid macaroon
 */
int dav_ns_init_info_from_macaroon(dav_error **ret, dav_resource_private *info);

#endif // MOD_LCGDM_NS_MACAROONS_H
