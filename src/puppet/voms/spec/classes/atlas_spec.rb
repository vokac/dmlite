
require 'spec_helper'

describe 'voms::atlas' do
  let(:hiera_config) { 'spec/hiera.yaml' }
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts
      end
      it { is_expected.to compile.with_all_deps }
      it { is_expected.to contain_file('/etc/grid-security/vomsdir/atlas/lcg-voms2.cern.ch.lsc')
           .with_content(%r{^/DC=ch/DC=cern/OU=computers/CN=lcg-voms2.cern.ch$}) }
      it { is_expected.to contain_file('/etc/grid-security/vomsdir/atlas/lcg-voms2.cern.ch.lsc')
           .with_content(%r{^/DC=ch/DC=cern/CN=CERN Grid Certification Authority$}) }
      it { is_expected.to contain_file('/etc/vomses/atlas-lcg-voms2.cern.ch')
           .with_content(%r{^"atlas" "lcg-voms2.cern.ch" "15001" "/DC=ch/DC=cern/OU=computers/CN=lcg-voms2.cern.ch" "atlas" "24"})}
    end
  end
end

