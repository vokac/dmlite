# Class defining the dream VO, as seen by the VOMS service.
#
# Takes care of all the required setup to enable access to the DREAM VO
# (users and services) in a grid enabled machine.
#
# == Examples
#
# Simply enable this class:
#   class{'voms::dream':}
#
# == Authors
#
# CERN IT/PS/PES <it-dep-ps-pes@cern.ch>

class voms::dream {
  voms::client{'dream':
    servers => [
      {
        server => 'voms.hpcc.ttu.edu',
        port   => '15004',
        dn     => '/DC=com/DC=DigiCert-Grid/O=Open Science Grid/OU=Services/CN=voms/voms.hpcc.ttu.edu',
        ca_dn  => '/DC=com/DC=DigiCert-Grid/O=DigiCert Grid/CN=DigiCert Grid CA-1',
      },
    ],
  }
}
