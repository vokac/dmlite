class voms::admin::install (
  $adminpkgs = $voms::params::adminpkgs,
  $tomcatuser = $voms::params::tomcatuser,
  $tomcatservice = $voms::params::tomcatservice,
) inherits voms::params {

  package{$adminpkgs:
    ensure  => present,
  }

}
