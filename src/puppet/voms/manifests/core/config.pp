class voms::core::config {

  file{'/etc/voms':
    ensure  => directory,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    purge   => true,
    recurse => true,
  }

  file{'/etc/voms/.globus':
    ensure  => directory,
    require => File['/etc/voms'],
  }
  # The host key must be owned by the voms user.


  file{'/etc/voms/.globus/usercert.pem':
    ensure => file,
    source => 'file:///etc/grid-security/hostcert.pem',
    owner  => voms,
    group  => voms,
    mode   => '0644',
  }
  file{'/etc/voms/.globus/userkey.pem':
    ensure    => file,
    source    => 'file:///etc/grid-security/hostkey.pem',
    owner     => voms,
    group     => voms,
    show_diff => false,
    mode      => '0600',
  }

  file {'/usr/lib64/voms':
    ensure  => link,
    target  => '/usr/lib64',
    replace => false,
    require => Package['voms-mysql-plugin'],
  }
}
