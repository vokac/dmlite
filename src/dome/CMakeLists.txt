cmake_minimum_required (VERSION 2.6)

include_directories(${Boost_INCLUDE_DIR} ${MYSQL_INCLUDE_DIR} ${DAVIX_PKG_INCLUDE_DIRS} ${XROOTD_INCLUDE_DIR})

set(Dome_SOURCES DomeGenQueue.cpp
                 DomeXrdHttp.cpp
                 DomeCore.cpp
                 DomeCoreXeq.cpp
                 DomeReq.cpp
                 DomeMysql.cpp
                 DomeMysql_cns.cpp
                 DomeMysql_authn.cpp
                 DomeStatus.cpp
                 DomeMetadataCache.cpp
                 ../utils/MySqlPools.cpp
                 ../utils/MySqlWrapper.cpp
                 ../utils/Config.cc
                 DomeLog.cpp
                 ${DMLITE_UTILS_SOURCES})


include_directories(${DAVIX_PKG_INCLUDE_DIRS})

set ( CMAKE_CXX_FLAGS "-Wall ${CMAKE_CXX_FLAGS}" )
add_library           (dome-5 SHARED ${Dome_SOURCES} ${DMLITE_UTILS_SOURCES} ${DMLITE_DAVIX_POOL_SOURCES})
target_link_libraries (dome-5 ${XROOTD_LIBRARIES} ${XROOTD_LIBRARIES_HTTP} ${DAVIX_PKG_LIBRARIES} ${MYSQL_LIBRARIES} ${Boost_LIBRARIES} crypto dmlite pthread dl)


add_executable        (dome-checksum DomeChecksum.cpp)
target_link_libraries (dome-checksum z crypto ssl ${DAVIX_PKG_LIBRARIES})

# Install
install (TARGETS dome-5
         DESTINATION            ${INSTALL_PFX_LIB}
         PERMISSIONS            OWNER_EXECUTE OWNER_WRITE OWNER_READ
                                GROUP_EXECUTE GROUP_READ
                                WORLD_EXECUTE WORLD_READ )

install (TARGETS dome-checksum
         DESTINATION            ${INSTALL_PFX_BIN}
         PERMISSIONS            OWNER_EXECUTE OWNER_WRITE OWNER_READ
                                GROUP_EXECUTE GROUP_READ
                                WORLD_EXECUTE WORLD_READ )
# tests
if (BUILD_TESTS STREQUAL "ON" AND CPPUNIT_FOUND)
  add_subdirectory (tests)
endif()
