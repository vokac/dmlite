/*
 * Copyright 2017 CERN
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */



/** @file  DomeXrdHttp.cpp
 * @brief  loading Dome as an Xrootd plugin
 * @author Fabrizio Furano
 * @date   May 2017
 */

#include "DomeLog.h"
#include "DomeCore.h"
#include <iostream>
#include <stdlib.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <unistd.h>
#include <XrdHttp/XrdHttpExtHandler.hh>
#include <XrdVersion.hh>

using namespace std;

class DomeXrdHttp: public XrdHttpExtHandler {
private:
  DomeCore core;
public:
  virtual bool MatchesPath(const char *verb, const char *path);
  virtual int Init(const char *cfgfile);
  virtual int ProcessReq(XrdHttpExtReq &);
};

bool DomeXrdHttp::MatchesPath(const char *verb, const char *path) {
  // Returning true this plugin takes control of all the XrdHttp activity
  // also in illegal paths, which can then be rejected with an error afterwards
  //
  // If we return false, XrdHttp will think that it's a data serving path
  // which may not be the case
  return true;
}

int DomeXrdHttp::ProcessReq(XrdHttpExtReq &xreq) {
  DomeReq dreq(xreq);
  int r = dreq.TakeRequest();
  if (r)
    return r;
  
  // If we are here and the incoming path is not recognized
  // as domehead, domedisk or whatever useful has been configured
  // then the client must be rejected
  

  std::string pfx = CFG->GetString("glb.auth.urlprefix", (char *)"");
  if (pfx.empty()) {
    dreq.SendSimpleResp(403, "No path allowed.");
    return -1;
  }
  
  if (dreq.object.compare(0, pfx.size(), pfx))  {
    dreq.SendSimpleResp(403, SSTR("Illegal path '" << dreq.object << "'"));
    return -1;
  }
    
    
  
  return core.processreq(dreq);
}

int DomeXrdHttp::Init(const char *cfgfile) {
  const char *c = cfgfile;
  
  if (!c || !c[0]) {
    c = getenv("DOME_CFGFILE");
    
    if (!c) {
      cerr << "Config file not provided in the initialization." << endl;
      cerr << "  Alternatively, set the envvar $DOME_CFGFILE" << endl;
      return -1;
    }
    
  }
  
  
  cout << "Welcome to dome" << endl;
  cout << "Cfg file: " << c << endl;
  
  
  domelogmask = Logger::get()->getMask(domelogname);
  
  
  if ( core.init(c) ) {
    cout << "Cannot start :-(" << endl;
    return -1;
  }
  
  return 0;
}




 









/******************************************************************************/
/*                    X r d H t t p G e t S e c X t r a c t o r               */
/******************************************************************************/

XrdHttpExtHandler *XrdHttpGetExtHandler(XrdHttpExtHandlerArgs)
{
  
  XrdHttpExtHandler *h = new DomeXrdHttp();
  if (h->Init(parms))
    return NULL;
  
  return (XrdHttpExtHandler *)h;
  
}

// This is the macro that declares the xrootd version this plugin is compliant with
// We only need to pass the name of the hook function and a name for logging.
// The version numbers actually are taken automatically at compile time.
XrdVERSIONINFO(XrdHttpGetExtHandler,domexrdhttp) 



