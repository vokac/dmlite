/// @file    core/dummy/DummyPool.cpp
/// @brief   DummyPoolManager implementation.
/// @details It makes sense as a base for other decorator plug-ins.
/// @author  Alejandro Álvarez Ayllón <aalvarez@cern.ch>
#include <dmlite/cpp/dummy/DummyPool.h>

using namespace dmlite;



/// Little of help here to avoid redundancy
#define DELEGATE(method, ...) \
if (this->decorated_ == NULL)\
  throw DmException(DMLITE_SYSERR(ENOSYS),\
                    "There is no plugin in the stack that implements "#method);\
this->decorated_->method(__VA_ARGS__);


/// Little of help here to avoid redundancy
#define DELEGATE_RETURN(method, ...) \
if (this->decorated_ == NULL)\
  throw DmException(DMLITE_SYSERR(ENOSYS),\
                    "There is no plugin in the stack that implements "#method);\
return this->decorated_->method(__VA_ARGS__);



DummyPoolManager::DummyPoolManager(PoolManager* decorated)  
{
  this->decorated_ = decorated;
}



DummyPoolManager::~DummyPoolManager()
{
  delete this->decorated_;
}



void DummyPoolManager::setStackInstance(StackInstance* si)  
{
  BaseInterface::setStackInstance(this->decorated_, si);
}



void DummyPoolManager::setSecurityContext(const SecurityContext* ctx)  
{
  BaseInterface::setSecurityContext(this->decorated_, ctx);
}



std::vector<Pool> DummyPoolManager::getPools(PoolAvailability availability)  
{
  DELEGATE_RETURN(getPools, availability);
}



Pool DummyPoolManager::getPool(const std::string& poolname)  
{
  DELEGATE_RETURN(getPool, poolname);
}



void DummyPoolManager::newPool(const Pool& pool)  
{
  DELEGATE(newPool, pool);
}



void DummyPoolManager::updatePool(const Pool& pool)  
{
  DELEGATE(updatePool, pool);
}



void DummyPoolManager::deletePool(const Pool& pool)  
{
  DELEGATE(deletePool, pool);
}



Location DummyPoolManager::whereToRead(ino_t inode)  
{
  DELEGATE_RETURN(whereToRead, inode);
}



Location DummyPoolManager::whereToRead(const std::string& path)  
{
  DELEGATE_RETURN(whereToRead, path);
}



Location DummyPoolManager::whereToWrite(const std::string& path)  
{
  DELEGATE_RETURN(whereToWrite, path);
}




DmStatus DummyPoolManager::fileCopyPush(const std::string& localsrcpath, const std::string &remotedesturl, int cksumcheck, char *cksumtype, dmlite_xferinfo *progressdata) {
  DELEGATE_RETURN(fileCopyPush, localsrcpath, remotedesturl, cksumcheck, cksumtype, progressdata);
}


DmStatus DummyPoolManager::fileCopyPull(const std::string& localdestpath, const std::string &remotesrcurl, int cksumcheck, char *cksumtype, dmlite_xferinfo *progressdata) {
  DELEGATE_RETURN(fileCopyPull, localdestpath, remotesrcurl, cksumcheck, cksumtype, progressdata);
}




