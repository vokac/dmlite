#
# This module detects if gridsite is installed and determines where the
# include files and libraries are.
#
# This code sets the following variables:
# 
# GRIDSITE_LIBRARIES = full path to the gridsite libraries
# GRIDSITE_INCLUDE   = include dir to be used when using the gridsite library
# GRIDSITE_FOUND     = set to true if gridsite was found successfully
#
# GRIDSITE_LOCATION
#   setting this enables search for gridsite libraries / headers in this location


# -----------------------------------------------------
# GRIDSITE Libraries
# -----------------------------------------------------
find_library(GRIDSITE_LIBRARIES
    NAMES gridsite
    HINTS ${GRIDSITE_LOCATION}/lib ${GRIDSITE_LOCATION}/lib64 
          ${GRIDSITE_LOCATION}/lib32
    DOC "The main gridsite library"
)

# -----------------------------------------------------
# GRIDSITE Include Directories
# -----------------------------------------------------
find_path(GRIDSITE_INCLUDE_DIR
    NAMES gridsite.h
    HINTS ${GRIDSITE_LOCATION} ${GRIDSITE_LOCATION}/include
          ${GRIDSITE_LOCATION}/include/*
    DOC "The gridsite include directory"
)

# -----------------------------------------------------
# handle the QUIETLY and REQUIRED arguments and set GSOAP_FOUND to TRUE if 
# all listed variables are TRUE
# -----------------------------------------------------
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(gridsite DEFAULT_MSG GRIDSITE_LIBRARIES 
    GRIDSITE_INCLUDE_DIR)
mark_as_advanced(GRIDSITE_INCLUDE_DIR GRIDSITE_LIBRARIES)
