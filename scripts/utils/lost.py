#!/usr/bin/python2
################################################################################
## Original script is now integrated in the dmliteshell which should be used  ##
## instead of calling this legacy but fully backward compatible CLI interface ##
################################################################################
#
# Find and deal with DPM lost and dark files (DB vs. filesystem inconsistencies)
#
# usage:
#   python lost.py --help
# examples:
#   python lost.py --verbose --processes 10 --stat-types=dark --fix-dark --all --include-fs-rdonly &> dpm-cleaner.out
#
import sys
from dmliteshell import lost

lost._log.warn("Calling directly %s is deprecated, use `dmlite-shell -e 'dbck lost'`" % sys.argv[0])
sys.exit(lost.main(sys.argv))
