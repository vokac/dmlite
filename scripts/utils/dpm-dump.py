#!/usr/bin/python2
################################################################################
## Original script is now integrated in the dmliteshell which should be used  ##
## instead of calling this legacy but fully backward compatible CLI interface ##
################################################################################
#
# Script for extracting file information from dpm database and converting
# the result into text, json or xml storage dump
#
# Erming Pei, 2009/11/13
# Tomas Kouba, 2012/11/16
# Dennis van Dok, 2015/07/03
# Alessandra Forti, 2015/10/14, 2015/11/18
# Eygene Ryabinkin, 2016
# Georgios Bitzes + Fabrizio Furano, 2016
# Petr Vokac, 2018/12/31
#
import sys
from dmliteshell import dump

dump._log.warn("Calling directly %s is deprecated, use `dmlite-shell -e 'dump'`" % sys.argv[0])
sys.exit(dump.main(sys.argv))
