#!/usr/bin/python2
################################################################################
## Original script is now integrated in the dmliteshell which should be used  ##
## instead of calling this legacy but fully backward compatible CLI interface ##
################################################################################
#
# A script that calculates the space occupied by files in every directory
# and can set the metadata filesize field with it, for the first N levels
#
# This script can directly update information in the database and not all
# operation leads to consistent results if you don't stop all DPM daemons
# before executing this script (see help screen for more details and examples)
#
# March 2015 - Fabrizio Furano - CERN IT/SDC - furano@cern.ch
# January 2019 - Petr Vokac - petr.vokac@cern.ch
#
#
# Usage:
# * get the help screen with all available options
#   dmlite-mysql-dirspaces [-h] [--help]
#
import sys
from dmliteshell import dbck

dbck._log.warn("Calling directly %s is deprecated, use `dmlite-shell -e 'dbck'`" % sys.argv[0])
sys.exit(dbck.main(sys.argv))
