/// @file  plugin_mock.h
/// @brief Mock plugin for the C tests.
/// @note  We rely on the BuiltInAuth for the authentication mechanism!
#ifndef PLUGIN_MOCK_H
#define	PLUGIN_MOCK_H

#include <dmlite/cpp/io.h>
#include <dmlite/cpp/poolmanager.h>
#include <dmlite/cpp/dummy/DummyCatalog.h>
#include <dmlite/cpp/dummy/DummyPool.h>
#include <map>

namespace dmlite {

  struct MockDirectory {
    DIR*         d;
    ExtendedStat holder;
  };

  /// Catalog mock plugin (uses FS calls)
  class MockCatalog: public DummyCatalog {
   private:
    typedef std::map<std::string, Replica> RfnReplicaType;
    typedef std::map<ino_t, RfnReplicaType> InoReplicasType;
    InoReplicasType replicas;

   public:
    MockCatalog();
    ~MockCatalog();

    std::string getImplId() const throw ();

    void        changeDir     (const std::string&)  ;
    std::string getWorkingDir (void)                ;

    ExtendedStat extendedStat(const std::string&, bool)  ;
    DmStatus extendedStat(ExtendedStat &xstat, const std::string&, bool)  ;

    Directory*    openDir (const std::string&)  ;
    void          closeDir(Directory*)          ;
    ExtendedStat* readDirx(Directory*)          ;

    mode_t umask    (mode_t)                           throw ();
    void   setMode  (const std::string&, mode_t)        ;
    void   makeDir  (const std::string&, mode_t)        ;
    void   create   (const std::string&, mode_t)        ;
    void   rename   (const std::string&, const std::string&)  ;
    void   removeDir(const std::string&)                      ;
    void   unlink   (const std::string&)                      ;

    void addReplica   (const Replica&)  ;
    void deleteReplica(const Replica&)  ;
    std::vector<Replica> getReplicas(const std::string&)  ;

    Replica getReplicaByRFN(const std::string& rfn)  ;
    void    updateReplica(const Replica& replica)  ;
  };

  /// Pool mock plugin
  class MockPoolManager: public DummyPoolManager {
   public:
     MockPoolManager();
     ~MockPoolManager();

     std::string getImplId() const throw();

     std::vector<Pool> getPools(PoolAvailability availability)  ;
     void newPool(const Pool&)  ;
     void updatePool(const Pool&)  ;
     void deletePool(const Pool&)  ;

     Location whereToRead (const std::string& path)  ;
     Location whereToRead (ino_t)                    ;
     Location whereToWrite(const std::string& path)  ;
  };

  /// Mock IO handler
  class MockIOHandler: public IOHandler {
    private:
     char     content[1024];
     off_t    p;
     bool     closed;
    public:
     MockIOHandler();
     ~MockIOHandler();

     void    close(void)  ;
     size_t  read (char* buffer, size_t count)  ;
     size_t  write(const char* buffer, size_t count)  ;
     void    seek (off_t offset, Whence whence)  ;
     off_t   tell (void)  ;
     void    flush(void)  ;
     bool    eof  (void)  ;
  };

  /// Mock IO Driver
  class MockIODriver: public IODriver {
   public:
     ~MockIODriver();

     std::string getImplId() const throw();
     void setSecurityContext(const SecurityContext*)  ;
     void setStackInstance(StackInstance* si)  ;

     IOHandler* createIOHandler(const std::string& pfn,
                                int flags,
                                const Extensible& extras,
                                mode_t mode)  ;

     void doneWriting(const Location& loc)  ;
  };

  /// Factory for mock implementation
  class MockFactory: public CatalogFactory,
                     public IODriverFactory,
                     public PoolManagerFactory {
   public:
    ~MockFactory();

    std::string implementedPool() throw();

    void configure(const std::string&, const std::string&)  ;

    Catalog*     createCatalog(PluginManager*)      ;
    IODriver*    createIODriver(PluginManager*)     ;
    PoolManager* createPoolManager(PluginManager*)  ;
  };

};

#endif	// PLUGIN_MOCK_H
