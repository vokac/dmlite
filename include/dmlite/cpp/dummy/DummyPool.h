/// @file    include/dmlite/cpp/dummy/DummyPool.h
/// @brief   A dummy plugin that just delegates calls to a decorated one.
/// @details It makes sense as a base for other decorator plug-ins.
/// @author  Alejandro Álvarez Ayllón <aalvarez@cern.ch>
#ifndef DMLITE_CPP_DUMMY_POOL_H
#define DMLITE_CPP_DUMMY_POOL_H

#include "../poolmanager.h"

namespace dmlite {

  class DummyPoolManager: public PoolManager {
   public:
    DummyPoolManager(PoolManager* decorated)  ;
    virtual ~DummyPoolManager();

    virtual void setStackInstance(StackInstance*)  ;
    virtual void setSecurityContext(const SecurityContext*)  ;

    virtual std::vector<Pool> getPools(PoolAvailability availability)  ;

    virtual Pool getPool(const std::string& poolname)  ;
    
    virtual void newPool(const Pool& pool)  ;
    virtual void updatePool(const Pool& pool)  ;
    virtual void deletePool(const Pool& pool)  ;

    virtual Location whereToRead(ino_t inode)  ;
    virtual Location whereToRead(const std::string& path)  ;

    virtual Location whereToWrite(const std::string& path)  ;

    virtual DmStatus fileCopyPush(const std::string& localsrcpath, const std::string &remotedesturl, int cksumcheck, char *cksumtype, dmlite_xferinfo *progressdata)  ;
    
    virtual DmStatus fileCopyPull(const std::string& localdestpath, const std::string &remotesrcurl, int cksumcheck, char *cksumtype, dmlite_xferinfo *progressdata)  ;
    
   protected:
    PoolManager* decorated_;
  };

};

#endif // DMLITE_DUMMYPOOL_H
